package bignum

import (
	"fmt"
	"math"
	"math/big"
	"strings"

	itn "gitlab.com/alexia.shaowei/bignum/internal"
)

type bignumber struct {
	integer *big.Int
	exp     int32
}

func (ref *bignumber) initIfNil() {
	if ref.integer == nil {
		ref.integer = new(big.Int)
	}
}

func (ref bignumber) align(exp int32) bignumber {
	ref.initIfNil()

	if ref.exp == exp {
		return bignumber{
			new(big.Int).Set(ref.integer),
			ref.exp,
		}
	}

	diff := math.Abs(float64(exp) - float64(ref.exp))
	val := new(big.Int).Set(ref.integer)

	expPre := new(big.Int).Exp(big.NewInt(10), big.NewInt(int64(diff)), nil)
	if exp > ref.exp {
		val = val.Quo(val, expPre)
	} else if exp < ref.exp {
		val = val.Mul(val, expPre)
	}

	return bignumber{
		integer: val,
		exp:     exp,
	}
}

func (ref bignumber) qr(y bignumber, precision int32) (bignumber, bignumber) {
	ref.initIfNil()
	y.initIfNil()
	if y.integer.Sign() == 0 {
		panic(itn.PANIC_DIVIDED_BY_0)
	}

	scale := -precision
	rE := int64(ref.exp - y.exp - scale)
	if rE > math.MaxInt32 || rE < math.MinInt32 {
		panic(itn.PANIC_OVERFLOW_Q_R)
	}

	var rest int32

	var iX big.Int
	var iY big.Int
	var iE big.Int
	if rE < 0 {
		iX = *ref.integer
		iE.SetInt64(-rE)

		iY.Exp(big.NewInt(10), &iE, nil)
		iY.Mul(y.integer, &iY)

		rest = ref.exp

	} else {
		iE.SetInt64(rE)
		iX.Exp(big.NewInt(10), &iE, nil)
		iX.Mul(ref.integer, &iX)

		iY = *y.integer

		rest = scale + y.exp
	}

	var q, r big.Int
	q.QuoRem(&iX, &iY, &r)

	qVal := bignumber{
		integer: &q,
		exp:     scale,
	}

	rVal := bignumber{
		integer: &r,
		exp:     rest,
	}

	return qVal, rVal
}

func (ref bignumber) compare(y bignumber) int {
	ref.initIfNil()
	y.initIfNil()

	if ref.exp == y.exp {
		return ref.integer.Cmp(y.integer)
	}

	rX, rY := alignScale(ref, y)

	return rX.integer.Cmp(rY.integer)
}

func (ref bignumber) abs() bignumber {
	ref.initIfNil()
	dVal := new(big.Int).Abs(ref.integer)
	return bignumber{
		integer: dVal,
		exp:     ref.exp,
	}
}

func (ref bignumber) string(trimZeros bool) string {
	if ref.exp >= 0 {
		return ref.align(0).integer.String()
	}

	abs := new(big.Int).Abs(ref.integer)
	str := abs.String()

	var intZone string
	var fractionalZone string

	eI := int(ref.exp)
	if len(str) > -eI {
		intZone = str[:len(str)+eI]
		fractionalZone = str[len(str)+eI:]

	} else {
		intZone = itn.NIL

		n := -eI - len(str)
		fractionalZone = strings.Repeat(itn.NIL, n) + str
	}

	if trimZeros {
		i := len(fractionalZone) - 1
		for ; i >= 0; i-- {
			if fractionalZone[i] != itn.CHAR0 {
				break
			}
		}

		fractionalZone = fractionalZone[:i+1]
	}

	num := intZone
	if len(fractionalZone) > 0 {
		num += itn.DECIMALPOINT + fractionalZone
	}

	if ref.integer.Sign() < 0 {
		return itn.MINUS + num
	}

	return num
}

func (ref bignumber) String() string {
	return ref.string(true)
}

func New(strNum string) (bignumber, error) {
	l := len(strNum)
	orgVal := strNum
	var strInt string
	var exp int64

	ptrIdx := strings.Index(strNum, itn.DECIMALPOINT)
	if ptrIdx < 0 {
		strInt = strNum
	} else {
		strInt = strings.Replace(strNum, itn.DECIMALPOINT, itn.EMPTY, 1)
		exp = -int64(l - ptrIdx - 1)
	}

	eVal := new(big.Int)
	_, ok := eVal.SetString(strInt, 10)
	if !ok {
		return bignumber{}, fmt.Errorf(itn.PANIC_CONVERT_BN_FAIL, strNum)
	}

	if exp > math.MaxInt32 || exp < math.MinInt32 {
		return bignumber{}, fmt.Errorf(itn.PANIC_CONVERT_BN_FRACTIONAL, orgVal)
	}

	return bignumber{
		integer: eVal,
		exp:     int32(exp),
	}, nil
}
