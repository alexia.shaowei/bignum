package main

import (
	"fmt"

	bn "gitlab.com/alexia.shaowei/bignum"
)

func main() {
	fmt.Println("main::main()")

	xn := "-9.9999165418915618478561515789415617489489521561"
	yn := "-333.0041111561894894156156156141"

	x, _ := bn.New(xn)
	y, _ := bn.New(yn)

	fmt.Println(x.Add(y).PrecisionRoundDown(10))
	fmt.Println(x.Subtract(y).PrecisionRoundDown(10))
	fmt.Println(x.Multiply(y).PrecisionRoundDown(10))
	fmt.Println(x.Divide(y, 11))

	zn := "15618.99999984110518979999"
	z, _ := bn.New(zn)
	fmt.Println(z.PrecisionRoundDown(5)) // 15618.99999
	fmt.Println(z.PrecisionRoundOff(5))  // 15619.00000

	fmt.Println("main::main() -> done")
}
