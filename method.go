package bignum

import (
	"fmt"
	"math"
	"math/big"
	"strings"

	itn "gitlab.com/alexia.shaowei/bignum/internal"
)

func (ref bignumber) Add(y bignumber) bignumber {
	rX, rY := alignScale(ref, y)

	sum := new(big.Int).Add(rX.integer, rY.integer)
	return bignumber{
		integer: sum,
		exp:     rX.exp,
	}
}

func (ref bignumber) Subtract(y bignumber) bignumber {
	rX, rY := alignScale(ref, y)

	difference := new(big.Int).Sub(rX.integer, rY.integer)
	return bignumber{
		integer: difference,
		exp:     rX.exp,
	}
}

func (ref bignumber) Multiply(y bignumber) bignumber {
	exp64 := int64(ref.exp) + int64(y.exp)
	if exp64 > math.MaxInt32 || exp64 < math.MinInt32 {
		panic(fmt.Sprintf("exp %v overflows an int32", exp64))
	}

	product := new(big.Int).Mul(ref.integer, y.integer)
	return bignumber{
		integer: product,
		exp:     int32(exp64),
	}
}

func (ref bignumber) Divide(y bignumber, precision int32) bignumber {
	quo, rem := ref.qr(y, precision)
	var iRem big.Int
	iRem.Abs(rem.integer)
	iRem.Lsh(&iRem, 1)

	rV := bignumber{
		integer: &iRem,
		exp:     rem.exp + precision,
	}

	var cmp = rV.compare(y.abs())

	if cmp < 0 {
		return quo
	}

	i := bignumber{
		integer: big.NewInt(1),
		exp:     -precision,
	}

	if ref.integer.Sign()*y.integer.Sign() < 0 {
		return quo.Subtract(i)
	}

	return quo.Add(i)
}

func (ref bignumber) PrecisionRoundDown(precision int) string {
	val := ref.string(false)

	if precision <= 0 {
		return val
	}

	pIdx := strings.Index(val, itn.DECIMALPOINT)

	if pIdx < 0 {
		return val + itn.DECIMALPOINT + strings.Repeat(itn.NIL, precision)
	}

	tailLen := len(val) - pIdx - 1
	if tailLen < precision {
		return val + strings.Repeat(itn.NIL, (precision-tailLen))
	}

	if tailLen > precision {
		return val[:precision+pIdx+1]
	}

	return val
}

func (ref bignumber) PrecisionRoundOff(precision int) string {
	val := ref.string(false)

	if precision <= 0 {
		return val
	}

	pIdx := strings.Index(val, itn.DECIMALPOINT)

	if pIdx < 0 {
		return val + itn.DECIMALPOINT + strings.Repeat(itn.NIL, precision)
	}

	tailLen := len(val) - pIdx - 1
	if tailLen < precision {
		return val + strings.Repeat(itn.NIL, (precision-tailLen))
	}

	if tailLen > precision {
		pVal := []byte(val)

		if pVal[pIdx+precision+1] >= itn.CHAR5 {
			addendNum, _ := New(itn.NILH + strings.Repeat(itn.NIL, precision-1) + itn.STR1)

			var num string
			if ref.integer.Sign() < 0 {
				num = ref.Subtract(addendNum).string(false)
			} else {
				num = ref.Add(addendNum).string(false)
			}

			return num[:precision+pIdx+1]
		}

		return val[:precision+pIdx+1]
	}

	return val
}
