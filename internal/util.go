package bncalculator

import (
	"errors"
	"strconv"
	"strings"
)

func paddingPxNil(val string, vl int) string {
	var sb strings.Builder
	for vl > 0 {
		sb.WriteString(NIL)
		vl--
	}

	sb.WriteString(val)

	return sb.String()
}

func paddingSxNil(val string, vl int) string {
	var sb strings.Builder

	sb.WriteString(val)
	for vl > 0 {
		sb.WriteString(NIL)
		vl--
	}

	return sb.String()
}

func paddingNil(val string, vl int, position int) string {
	if position == PREFIX {
		return paddingPxNil(val, vl)
	}

	return paddingSxNil(val, vl)
}

func alignNil(x, y string) (string, string) {
	xl := len(x)
	yl := len(y)

	if xl > yl {
		y = paddingPxNil(y, xl-yl)

	} else if xl < yl {
		x = paddingPxNil(x, yl-xl)

	}

	return x, y
}

func convIntSliceToString(val []int) string {
	var sb strings.Builder

	for _, v := range val {
		sb.WriteString(strconv.Itoa(v))
	}

	return sb.String()
}

func rmvPxNil(val string) string {
	return strings.TrimLeft(val, NIL)
}

func rmvSxNil(val string) string {
	return strings.TrimRight(val, NIL)
}

func trimIntegerNil(val []int) string {
	num := convIntSliceToString(val)

	value := rmvPxNil(num)
	if len(value) == 0 {
		return NIL
	}

	return value
}

func trimNum(x string) string {

	isNegative := false
	if x[0] == HEX_PLUSSIGN {
		x = x[1:]

	} else if x[0] == HEX_MINUSSIGN {
		x = x[1:]
		isNegative = true
	}

	if strings.Contains(x, DECIMALPOINT) {
		x = strings.Trim(x, NIL)
	} else {
		x = strings.TrimLeft(x, NIL)
	}

	if len(x) == 0 {
		return NIL
	}

	if x == DECIMALPOINT {
		return NIL
	}

	if x[0] == HEX_POINT {
		x = NIL + x
	}

	if x[len(x)-1] == HEX_POINT {
		x = x[:len(x)-1]
	}

	if isNegative {
		x = MINUS + x
	}

	return x
}

func AlignPos(x, y string) (string, string, error) {
	if x == y {
		return x, y, nil
	}

	xl := len(x)
	yl := len(y)

	xpdx := strings.Index(x, DECIMALPOINT)
	ypdx := strings.Index(y, DECIMALPOINT)

	xtl := 0
	ytl := 0
	xhl := xl
	yhl := yl

	if xpdx != -1 {
		xtl = xl - xpdx - 1
		xhl = xpdx
	}

	if ypdx != -1 {
		ytl = yl - ypdx - 1
		yhl = ypdx
	}

	offset := xtl - ytl

	var sb strings.Builder
	if offset > 0 {
		sb.WriteString(y)
		if ypdx == -1 {
			sb.WriteString(DECIMALPOINT)
		}

		for i := 0; i < offset; i++ {
			sb.WriteString(NIL)
		}

		y = sb.String()

	} else if offset < 0 {
		sb.WriteString(x)
		if xpdx == -1 {
			sb.WriteString(DECIMALPOINT)
		}

		for i := offset; i < 0; i++ {
			sb.WriteString(NIL)
		}

		x = sb.String()
	}

	sb.Reset()

	if xhl > yhl {
		for i := 0; i < (xhl - yhl); i++ {
			sb.WriteString(NIL)
		}
		sb.WriteString(y)

		y = sb.String()

	} else if yhl > xhl {
		for i := 0; i < (yhl - xhl); i++ {
			sb.WriteString(NIL)
		}
		sb.WriteString(x)

		x = sb.String()
	}

	if len(x) != len(y) {
		return EMPTY, EMPTY, errors.New("decimal point mismatch")
	}

	return x, y, nil
}

func alignPosition(x, y string) (string, string, int, error) {
	if x == y {
		pos := strings.Index(x, DECIMALPOINT)

		x = strings.ReplaceAll(x, DECIMALPOINT, EMPTY)
		y = strings.ReplaceAll(y, DECIMALPOINT, EMPTY)

		return x, y, pos, nil
	}

	xl := len(x)
	yl := len(y)

	xpdx := strings.Index(x, DECIMALPOINT)
	ypdx := strings.Index(y, DECIMALPOINT)

	xtl, ytl := 0, 0
	xhl, yhl := xl, yl

	if xpdx != -1 {
		xtl = xl - xpdx - 1
		xhl = xpdx
	}

	if ypdx != -1 {
		ytl = yl - ypdx - 1
		yhl = ypdx
	}

	left := xhl
	right := xtl
	if yhl > xhl {
		left = yhl
	}

	if ytl > xtl {
		right = ytl
	}

	ppos := right
	if xpdx == -1 && ypdx == -1 {
		ppos = -1
	}

	x = strings.ReplaceAll(x, DECIMALPOINT, EMPTY)
	y = strings.ReplaceAll(y, DECIMALPOINT, EMPTY)

	ln := left + right

	xbs := make([]byte, ln)
	for idx := range xbs {
		xbs[idx] = HEX0
	}

	ybs := make([]byte, ln)
	for idx := range ybs {
		ybs[idx] = HEX0
	}

	for pos, i := (left - xhl), 0; i < len(x); i++ {
		xbs[pos] = x[i]
		pos++
	}

	for pos, i := (left - yhl), 0; i < len(y); i++ {
		ybs[pos] = y[i]
		pos++
	}

	return string(xbs), string(ybs), ppos, nil
}

func ComparePositive(x, y string) int {
	if x == y {
		return EQUAL
	}

	xS := strings.Split(x, DECIMALPOINT)
	yS := strings.Split(y, DECIMALPOINT)

	xhval := strings.TrimLeft(xS[0], NIL)
	yhval := strings.TrimLeft(yS[0], NIL)
	xtval := EMPTY
	ytval := EMPTY

	if len(xS) > 1 {
		xtval = strings.TrimRight(xS[1], NIL)
	}

	if len(yS) > 1 {
		ytval = strings.TrimRight(yS[1], NIL)
	}

	xhl := len(xhval)
	yhl := len(yhval)

	if xhl > yhl {
		return GREATER
	}

	if xhl < yhl {
		return LESS
	}

	if xhl == yhl && xhval != yhval {
		if xhval > yhval {
			return GREATER
		}

		return LESS
	}

	if xtval == ytval {
		return EQUAL
	}

	if xtval > ytval {
		return GREATER
	}

	if xtval < ytval {
		return LESS
	}

	return EQUAL
}

func CompareAlignedVal(x, y string) (int, error) {
	if len(x) != len(y) {
		return OBSCURE, errors.New("confused argument")
	}

	for i := range x {
		if x[i] > y[i] {
			return GREATER, nil

		} else if x[i] < y[i] {
			return LESS, nil

		}
	}

	return EQUAL, nil
}

func compareAligned(x, y string) (int, error) {
	if len(x) != len(y) {
		return OBSCURE, errors.New("confused argument")
	}

	if x < y {
		return LESS, nil
	}

	if x > y {
		return GREATER, nil
	}

	return EQUAL, nil
}

func insertInt(pos int, val int, xInt []int) ([]int, error) {
	if pos > len(xInt) {
		return xInt, errors.New("fail to align decimal point")
	}

	xInt = append(xInt, 0)
	copy(xInt[pos+1:], xInt[pos:])
	xInt[pos] = val

	return xInt, nil
}

func analyzeProduct(x, y string) (string, string, int, bool) {
	xyMinus := (x[0] == HEX_MINUSSIGN) != (y[0] == HEX_MINUSSIGN)
	x = strings.ReplaceAll(x, MINUS, EMPTY)
	y = strings.ReplaceAll(y, MINUS, EMPTY)

	xpdx := strings.Index(x, DECIMALPOINT)
	ypdx := strings.Index(y, DECIMALPOINT)
	xOffset := 0
	yOffset := 0
	if xpdx != -1 {
		xOffset = len(x) - 1 - xpdx
	}

	if ypdx != -1 {
		yOffset = len(y) - 1 - ypdx
	}

	xyOffset := xOffset + yOffset

	x = strings.ReplaceAll(x, DECIMALPOINT, EMPTY)
	y = strings.ReplaceAll(y, DECIMALPOINT, EMPTY)

	return x, y, xyOffset, xyMinus
}

func insertPoint(val string, pos int) string {
	if len(val) <= pos {
		val = paddingPxNil(val, pos-len(val)+1)
	}

	return val[:len(val)-pos] + DECIMALPOINT + val[len(val)-pos:]
}

func deterMinus(x, y string) bool {
	return x[0] == HEX_MINUSSIGN && y[0] == HEX_MINUSSIGN
}
